<?php
namespace Itschrake\Itstagcloud\Controller;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use TYPO3\CMS\Core\Utility\DebugUtility;

/**
 * Description of TagController
 *
 * @author Bülent Aldede
 */
class TagController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController{
    //put your code here

    /**
     *
     * @var PageRepository
     */
    protected $pageRepository;

    /**
     *
     * @var $tagRepository
     */
    protected $tagRepository;

    public function initializeAction() {
        parent::initializeAction();
    }

    /**
     * @param \Itschrake\Itstagcloud\Domain\Repository\TagRepository $tagRepository
     * @return void
     */
    public function injectTagRepository(\Itschrake\Itstagcloud\Domain\Repository\TagRepository $tagRepository)
    {
        $this->tagRepository = $tagRepository;
    }

    public function listAction(){


        $ke_searchResultPage = $this->settings['resultPage'];
        //  DebugUtility::debug($ke_searchResultPage);
        $filters = $this->tagRepository->findAllFilters();

        $anzahlFilter = count($filters);
        $disabledFilters = $this->settings['disabledFilters'];

        // Die Filteroptionen von deaktivierten Filter(s)
        $disabledFilterOptions = array();

        if($disabledFilters != ''){
            $disabledFiltersArray = explode(',', $disabledFilters);
            foreach ($disabledFiltersArray as $disabledFilter){
                foreach ($filters as $filter){
                    if($disabledFilter == $filter['uid']){
                        $tempFilterOptions = explode(',', $filter['options']);
                        foreach ($tempFilterOptions as $tempFilterOption){
                            $disabledFilterOptions[] = $tempFilterOption;
                        }
                    }
                }
            }
        }

        $filterOptions = $this->tagRepository->findAllFilterOptions();


        foreach ($filterOptions as $i => $filterOption){
            foreach ($disabledFilterOptions as $disabledFilterOption){
                if($disabledFilterOption == $filterOption['uid']){
                    unset($filterOptions[$i]);
                }
            }
        }

        $pagesWithFilteroptionTags = $this->tagRepository->getAllPagesWithFilterTagsAsArray();

        /**
        \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($pagesWithFilteroptionTags);
        foreach ($pagesWithFilteroptionTags as $value){

        echo 'pagevalue '. $value['tx_kesearch_tags'] . '<br />';

        }
         *
         */
        $filterOptionsMitGewichtung = $this->rechneTagsMitGewichtung($filterOptions, $pagesWithFilteroptionTags, $filters);

        $words = json_encode($filterOptionsMitGewichtung);

        $this->view->assign('filteroptions', $filterOptionsMitGewichtung);
        $this->view->assign('words', $words);
        $this->view->assign('resultPage', $ke_searchResultPage);
    }


    protected function rechneTagsMitGewichtung($filterOptions, $pagesWithFilteroptionTags, $filters){
        $gesamtGewicht = 0;
        $tagElements = array();
        foreach ($pagesWithFilteroptionTags as $value){
            $tagValues = explode(",", $value['tx_kesearch_tags']);
            foreach ($tagValues as $tagValue){
                $tagElements[] = $tagValue;
            }
        }
        /**
        print "<pre>";
        print_r($tagElements);
        print "</pre>";
        echo 'Anzahl von Tags '. count($tagElements);
         *
         */
        $gesamtGewicht = count($tagElements);
        $tempGewicht = 0;
        $index = 0;
        $filterOptionMitGewichtung = array();
        foreach ($filterOptions as $filterOption){
           $filterOptionMitGewichtung[$index]['uid'] = $filterOption['uid'];
           $filterOptionMitGewichtung[$index]['pid'] = $filterOption['pid'];
           $filterOptionMitGewichtung[$index]['title'] = $filterOption['title'];
           foreach ($tagElements as $tagElement){
               if($tagElement == $filterOptionMitGewichtung[$index]['uid']){
                 //   echo 'aktuell TepmElement = '.$tagElement .' und aktuelle filterOptionMitGewichtung[uid] = '.$filterOptionMitGewichtung['uid'].'<br />';
                    $tempGewicht++;
               }
           }
           $filterOptionMitGewichtung[$index]['anzahlVorkommnisse'] = $tempGewicht;
           $filterOptionMitGewichtung[$index]['gewichtung'] = round($tempGewicht/$gesamtGewicht*100, 2);
           foreach ($filters as $filter){
               $optionsInFilter = explode(",", $filter['options']);
               foreach ($optionsInFilter as $optionInFilter){
                   if($filterOptionMitGewichtung[$index]['uid'] == $optionInFilter){
                       $filterOptionMitGewichtung[$index]['filter']=$filter['uid'];
                   }
               }
           }
         //  echo 'Tempgewicht '. $tempGewicht .'<br />';
           $tempGewicht = 0;
           $index++;
        }
        /**
        print "<pre>";
        print_r($filterOptionMitGewichtung);
        print "</pre>";
         *
         */
        return $filterOptionMitGewichtung;
    }
}
