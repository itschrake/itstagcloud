<?php
namespace Itschrake\Itstagcloud\Domain\Repository;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TagRepository
 *
 * @author Bülent Aldede
 */
class TagRepository extends \TYPO3\CMS\Extbase\Persistence\Repository{
    //put your code here
    public function findAllFilterOptions(){

        $query = $this->createQuery();
		$query->statement( "SELECT uid, pid, title FROM tx_kesearch_filteroptions WHERE hidden=0 AND deleted=0" );
		$result = $query->execute(TRUE);  ### returnRawQueryResult:TRUE (default:false)
	//	\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump( $result, '$result in testSql' );
		return $result;	
    }  

    public function findAllFilters(){

        $query = $this->createQuery();
        $query->statement( "SELECT uid, pid, options FROM tx_kesearch_filters WHERE hidden=0 AND deleted=0" );
        $result = $query->execute(TRUE);  ### returnRawQueryResult:TRUE (default:false)
        //	\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump( $result, '$result in testSql' );
        return $result;
    }

    public function getAllPagesWithFilterTagsAsArray(){
            
        // Quck mal hier:  https://blog.bartlweb.net/2010/03/sql-abfrage-in-typo3-extension/
        //$select = $GLOBALS['TYPO3_DB']->exec_SELECTquery('uid, tx_kesearch_tags', 'pages', 'hidden = 0 AND deleted = 0 AND tx_kesearch_tags != NULL', '', '');
        //debug($select);
        
        //($queryBuilder = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('pages')->createQueryBuilder();
        //$queryBuilder->select('uid, tx_kesearch_tags')->from('pages');
        	$query = $this->createQuery();
		$query->statement( "SELECT title, uid, pid, tx_kesearch_tags from pages WHERE hidden=0 AND deleted=0 AND tx_kesearch_tags IS NOT NULL" );
		$result = $query->execute(TRUE);  ### returnRawQueryResult:TRUE (default:false)
	//	\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump( $result, '$result in testSql' );
		return $result;	
    }
}
