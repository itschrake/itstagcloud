<?php

namespace Itschrake\Itstagcloud\Domain\Model;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tag
 *
 * @author Bülent Aldede
 */
class Tag extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity{
    
    /**
     * tagName
     * 
     * @var string
     */
    protected $tagName;
    
    /**
     * tagUid
     * 
     * @var int 
     */
    protected $tagUid;
    
    /**
     * gewichtung
     * 
     * @var int
     */
    protected $gewichtung;
    
    /**
     *
     * linkurl
     * 
     * @var string 
     */
    protected $linkurl;
    
    /**
     * return the tagName
     * 
     * @return  string $tagName
     */
    function getTagName() {
        return $this->tagName;
    }
 
    /**
     * return the tagUid
     * 
     * @return int $tagUid
     */
    function getTagUid() {
        return $this->tagUid;
    }

    /**
     * return the gewichtung
     * 
     * @return int $gewichtung
     */
    function getGewichtung() {
        return $this->gewichtung;
    }

    /**
     * return the link
     * 
     * @return string $link
     */
    function getLinkurl() {
        return $this->linkurl;
    }

    /**
     * sets the tagName
     * 
     * @param string $tagName
     */
    function setTagName($tagName) {
        $this->tagName = $tagName;
    }

    
    /**
     * sets the tagUid
     * 
     * @param int $tagUid
     */
    function setTagUid($tagUid) {
        $this->tagUid = $tagUid;
    }


    /**
     * sets the gewichtung
     * 
     * @param int $gewichtung
     */
    function setGewichtung($gewichtung) {
        $this->gewichtung = $gewichtung;
    }

    /**
     * sets the linkurl
     * 
     * @param string $linkurl
     */
    function setLinkurl($linkurl) {
        $this->linkurl = $linkurl;
    }
}
